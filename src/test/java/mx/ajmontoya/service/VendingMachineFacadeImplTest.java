package mx.ajmontoya.service;

import mx.ajmontoya.dto.ProductDto;
import mx.ajmontoya.model.money.Bill;
import mx.ajmontoya.model.money.Coin;
import mx.ajmontoya.service.internal.CreditService;
import mx.ajmontoya.service.internal.FileLoader;
import mx.ajmontoya.service.internal.ProductService;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import java.math.BigDecimal;
import java.util.Arrays;

import static org.mockito.Mockito.*;

public class VendingMachineFacadeImplTest {
    @Mock
    ProductService productService;
    @Mock
    FileLoader fileLoader;
    @Mock
    CreditService creditService;
    @InjectMocks
    VendingMachineFacadeImpl vendingMachineFacadeImpl;

    @Before
    public void setUp() {
        MockitoAnnotations.initMocks(this);
    }

    @Test
    public void testUploadFromFile() throws Exception {
        when(productService.uploadProducts(any()))
                .thenReturn("uploadProductsResponse");
        when(fileLoader.uploadFromFile(anyString()))
                .thenReturn(Arrays.asList(new ProductDto("name", "code", new BigDecimal(0))));

        String result = vendingMachineFacadeImpl.uploadFromFile("filePath");
        Assert.assertEquals("replaceMeWithExpectedResult", result);
    }

    @Test
    public void testUploadFromURL() throws Exception {
        String result = vendingMachineFacadeImpl.uploadFromURL("url");
        Assert.assertEquals("replaceMeWithExpectedResult", result);
    }

    @Test
    public void testList() throws Exception {
        when(productService.listProducts()).thenReturn("listProductsResponse");

        String result = vendingMachineFacadeImpl.list();
        Assert.assertEquals("replaceMeWithExpectedResult", result);
    }

    @Test
    public void testAddCoins() throws Exception {
        when(creditService.addCoin(any())).thenReturn(new BigDecimal(0));

        String result = vendingMachineFacadeImpl.addCoins(Coin.CENT_1);
        Assert.assertEquals("replaceMeWithExpectedResult", result);
    }

    @Test
    public void testAddBills() throws Exception {
        when(creditService.addBill(any())).thenReturn(new BigDecimal(0));

        String result = vendingMachineFacadeImpl.addBills(Bill.DOLLAR_2);
        Assert.assertEquals("replaceMeWithExpectedResult", result);
    }

    @Test
    public void testPurchase() throws Exception {
        when(productService.isValidKey(anyString())).thenReturn(true);
        when(productService.getProduct(anyString())).thenReturn(new ProductDto("name", "code", new BigDecimal(0)));
        when(creditService.availableAmount()).thenReturn(new BigDecimal(0));
        when(creditService.isValidSubtraction(any())).thenReturn(true);
        when(creditService.subtractAmount(any())).thenReturn(new BigDecimal(0));

        String result = vendingMachineFacadeImpl.purchase("code");
        Assert.assertEquals("replaceMeWithExpectedResult", result);
    }

    @Test
    public void testChange() throws Exception {
        when(creditService.availableAmount()).thenReturn(new BigDecimal(0));

        String result = vendingMachineFacadeImpl.change();
        Assert.assertEquals("replaceMeWithExpectedResult", result);
    }

    @Test
    public void testCancel() throws Exception {
        when(creditService.availableAmount()).thenReturn(new BigDecimal(0));

        String result = vendingMachineFacadeImpl.cancel();
        Assert.assertEquals("replaceMeWithExpectedResult", result);
    }

    @Test
    public void testShowCredit() throws Exception {
        when(creditService.availableAmount()).thenReturn(new BigDecimal(0));

        String result = vendingMachineFacadeImpl.showCredit();
        Assert.assertEquals("replaceMeWithExpectedResult", result);
    }

    @Test
    public void testAskForAnotherPurchase() throws Exception {
        when(creditService.availableAmount()).thenReturn(new BigDecimal(0));

        String result = vendingMachineFacadeImpl.askForAnotherPurchase();
        Assert.assertEquals("replaceMeWithExpectedResult", result);
    }
}

//Generated with love by TestMe :) Please report issues and submit feature requests at: http://weirddev.com/forum#!/testme