package mx.ajmontoya.init;

import mx.ajmontoya.service.VendingMachineFacade;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import static org.mockito.Mockito.*;

public class CommanderTest {
    @Mock
    VendingMachineFacade vendingMachineFacade;
    @InjectMocks
    Commander commander;

    @Before
    public void setUp() {
        MockitoAnnotations.initMocks(this);
    }

    @Test
    public void testUpload() throws Exception {
        when(vendingMachineFacade.uploadFromFile(anyString())).thenReturn("uploadFromFileResponse");

        String result = commander.upload("file");
        Assert.assertEquals("replaceMeWithExpectedResult", result);
    }

    @Test
    public void testList() throws Exception {
        when(vendingMachineFacade.list()).thenReturn("listResponse");

        String result = commander.list();
        Assert.assertEquals("replaceMeWithExpectedResult", result);
    }

    @Test
    public void testAddCoins() throws Exception {
        when(vendingMachineFacade.addCoins(any())).thenReturn("addCoinsResponse");

        String result = commander.addCoins(0);
        Assert.assertEquals("replaceMeWithExpectedResult", result);
    }

    @Test
    public void testAddBills() throws Exception {
        when(vendingMachineFacade.addBills(any())).thenReturn("addBillsResponse");

        String result = commander.addBills(0);
        Assert.assertEquals("replaceMeWithExpectedResult", result);
    }

    @Test
    public void testAvailableCredit() throws Exception {
        when(vendingMachineFacade.showCredit()).thenReturn("showCreditResponse");

        String result = commander.availableCredit();
        Assert.assertEquals("replaceMeWithExpectedResult", result);
    }

    @Test
    public void testPurchaseProduct() throws Exception {
        when(vendingMachineFacade.purchase(anyString())).thenReturn("purchaseResponse");

        String result = commander.purchaseProduct("code");
        Assert.assertEquals("replaceMeWithExpectedResult", result);
    }

    @Test
    public void testChange() throws Exception {
        when(vendingMachineFacade.change()).thenReturn("changeResponse");

        String result = commander.change();
        Assert.assertEquals("replaceMeWithExpectedResult", result);
    }

    @Test
    public void testCancelOperation() throws Exception {
        when(vendingMachineFacade.cancel()).thenReturn("cancelResponse");

        String result = commander.cancelOperation();
        Assert.assertEquals("replaceMeWithExpectedResult", result);
    }
}

//Generated with love by TestMe :) Please report issues and submit feature requests at: http://weirddev.com/forum#!/testme