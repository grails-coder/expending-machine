package mx.ajmontoya.dto;

import com.opencsv.bean.CsvBindByName;
import com.opencsv.bean.CsvNumber;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.math.BigDecimal;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class ProductDto {

    @CsvBindByName(column = "item name")
    private String name;

    @CsvBindByName
    private String code;

    @CsvBindByName(locale = "en_US")
    @CsvNumber("$###,###.###")
    private BigDecimal cost;
}
