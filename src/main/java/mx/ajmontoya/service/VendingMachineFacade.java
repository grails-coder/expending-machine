package mx.ajmontoya.service;

import mx.ajmontoya.model.money.Bill;
import mx.ajmontoya.model.money.Coin;

public interface VendingMachineFacade {

    String uploadFromFile(String filePath);
    String uploadFromURL(String url);
    String list();
    String addCoins(Coin coin);
    String addBills(Bill bill);
    String purchase(String code);
    String change();
    String cancel();
    String showCredit();
    String askForAnotherPurchase();
}
