package mx.ajmontoya.service.internal;

import mx.ajmontoya.dto.ProductDto;

import java.util.List;

public interface ProductService {
    String uploadProducts(List<ProductDto> productDtos);

    String listProducts();

    boolean isValidKey(String code);

    ProductDto getProduct(String code);
}
