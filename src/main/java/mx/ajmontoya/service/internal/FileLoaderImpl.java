package mx.ajmontoya.service.internal;

import com.opencsv.bean.CsvToBeanBuilder;
import mx.ajmontoya.dto.ProductDto;

import java.io.IOException;
import java.io.Reader;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.List;

public class FileLoaderImpl implements FileLoader {
    @Override
    public List<ProductDto> uploadFromFile(final String filePath) throws IOException {
        try (
                Reader reader = Files.newBufferedReader(Paths.get(filePath))
        ) {
            return new CsvToBeanBuilder(reader)
                    .withType(ProductDto.class)
                    .withIgnoreLeadingWhiteSpace(true)
                    .build()
                    .parse();
        }
    }

    @Override
    public String uploadFromURL(final String url) {
        return null;
    }
}
