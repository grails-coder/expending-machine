package mx.ajmontoya.service.internal;

import mx.ajmontoya.dto.ProductDto;
import mx.ajmontoya.model.ProductItem;

import java.util.*;

public class ProductServiceImpl implements ProductService {

    private Map<String, ProductItem> products = new HashMap<>();

    @Override
    public String uploadProducts(List<ProductDto> productDtos) {

        //first off, remove old data
        products.clear();
        for (ProductDto item: productDtos) {
            products.put(item.getCode().trim(), new ProductItem(item.getName().trim(), item.getCost()));
        }

        return productDtos.size() + " products added";
    }

    @Override
    public String listProducts() {
        StringBuilder result = new StringBuilder();
        ArrayList<String> keySet = new ArrayList<>(products.keySet());

        if (keySet.isEmpty()) {
            return "No products were found";
        }

        // sort the keys so that iteration comes in order
        // consequently data output will be sorted
        Collections.sort(keySet);
        result
                .append("\n")
                .append("Code\tItem name\tCost")
                .append("\n")
                .append("------------------------------------------")
        ;
        for (String key: keySet) {
            ProductItem item = products.get(key);
            result
                    .append("\n")
                    .append(key)
                    .append("\t\t")
                    .append(item.getName())
                    .append("\t\t")
                    .append("$")
                    .append(item.getCost())
            ;

        }
        return result.toString();
    }

    @Override
    public boolean isValidKey(String key) {
        return products.containsKey(key);
    }

    @Override
    public ProductDto getProduct(String code) {
        ProductItem item = products.get(code);
        return new ProductDto(item.getName(),code, item.getCost());
    }
}
