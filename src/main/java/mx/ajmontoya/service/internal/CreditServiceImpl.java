package mx.ajmontoya.service.internal;

import mx.ajmontoya.model.money.Bill;
import mx.ajmontoya.model.money.Coin;

import java.math.BigDecimal;

public class CreditServiceImpl implements CreditService{

    private static final BigDecimal CENTS_FACTOR    = new BigDecimal(100);
    private BigDecimal creditAvailable              = BigDecimal.ZERO;

    @Override
    public BigDecimal addCoin(Coin coin) {
        return creditAvailable = creditAvailable.add(
                new BigDecimal(coin.getValue()).divide(CENTS_FACTOR)
        );
    }

    @Override
    public BigDecimal addBill(Bill bill) {
        return creditAvailable = creditAvailable.add(new BigDecimal(bill.getValue()));
    }

    @Override
    public BigDecimal availableAmount() {
        return creditAvailable;
    }

    @Override
    public boolean isValidSubtraction(BigDecimal amount) {
        return creditAvailable.subtract(amount).compareTo(BigDecimal.ZERO) >= 0;
    }

    @Override
    public BigDecimal subtractAmount(BigDecimal subtractAmount) {
        return creditAvailable = creditAvailable.subtract(subtractAmount);
    }
}
