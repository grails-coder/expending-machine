package mx.ajmontoya.service.internal;

import mx.ajmontoya.model.money.Bill;
import mx.ajmontoya.model.money.Coin;

import java.math.BigDecimal;

public interface CreditService {

    BigDecimal addCoin(Coin coin);

    BigDecimal addBill(Bill bill);

    BigDecimal availableAmount();

    boolean isValidSubtraction(BigDecimal amount);

    BigDecimal subtractAmount(BigDecimal subtractAmount);

}
