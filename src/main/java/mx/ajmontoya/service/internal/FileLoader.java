package mx.ajmontoya.service.internal;

import mx.ajmontoya.dto.ProductDto;

import java.io.IOException;
import java.util.List;

public interface FileLoader {
    List<ProductDto> uploadFromFile(String filePath) throws IOException;
    String uploadFromURL(String url);
}
