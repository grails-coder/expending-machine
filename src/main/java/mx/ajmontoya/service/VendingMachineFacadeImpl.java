package mx.ajmontoya.service;

import mx.ajmontoya.dto.ProductDto;
import mx.ajmontoya.model.money.Bill;
import mx.ajmontoya.model.money.Coin;
import mx.ajmontoya.service.internal.CreditService;
import mx.ajmontoya.service.internal.FileLoader;
import mx.ajmontoya.service.internal.ProductService;

import java.io.IOException;
import java.math.BigDecimal;
import java.util.List;

public class VendingMachineFacadeImpl implements VendingMachineFacade {

    private final ProductService productService;
    private final FileLoader fileLoader;
    private final CreditService creditService;

    public VendingMachineFacadeImpl(ProductService productService,
                                    FileLoader fileLoader,
                                    CreditService creditService){
        this.productService = productService;
        this.fileLoader = fileLoader;
        this.creditService = creditService;
    }

    @Override
    public String uploadFromFile(String filePath) {
        try {
            List<ProductDto> list = fileLoader.uploadFromFile(filePath);
            return productService.uploadProducts(list);
        } catch (IOException e) {
            return "File does not exist";
        }
    }

    @Override
    public String uploadFromURL(String url) {
        return "Uploading products from URL";
    }

    @Override
    public String list() {
        return productService.listProducts();
    }

    @Override
    public String addCoins(Coin coinValue) {
        return String.format("Current credit: $%s", creditService.addCoin(coinValue).toString());
    }

    @Override
    public String addBills(Bill billValue) {
        return String.format("Current credit: $%s", creditService.addBill(billValue).toString());
    }

    @Override
    public String purchase(String code) {
        if (!productService.isValidKey(code)) {
            return "Code not valid";
        }
        ProductDto product = productService.getProduct(code);
        if (creditService.isValidSubtraction(product.getCost())) {
            creditService.subtractAmount(product.getCost());
            return String.format("Vended product: (%s) %s at $%s %nChange amount is: $%s",
                    product.getCode(),
                    product.getName(),
                    product.getCost().toString(),
                    this.showCredit());
        }
        return String.format("Not sufficient credit, please add %s more cents",
                product.getCost().subtract(creditService.availableAmount())
        );

    }

    @Override
    public String change() {
        BigDecimal available = creditService.availableAmount();
        if (available.compareTo(BigDecimal.ZERO) >= 0) {
            return "No change available to return";
        } else {
            return String.format("Change amount returned is: $%s", available);
        }
    }

    @Override
    public String cancel() {
        BigDecimal available = creditService.availableAmount();
        if (available.compareTo(BigDecimal.ZERO) >= 0) {
            return "Operation cancelled.";
        } else {
            return String.format("Operation cancelled.%nChange amount returned is: $%s", available);
        }
    }

    @Override
    public String showCredit() {
        return creditService.availableAmount().toString();
    }

    @Override
    public String askForAnotherPurchase() {
        BigDecimal available = creditService.availableAmount();

        if (available.compareTo(BigDecimal.ZERO) < 0) {
            return "Would you like to buy another product?, type: purchase + [product_code]";
        }
        return "";
    }
}
