package mx.ajmontoya.exception;

public class BadFormatFileException extends RuntimeException {

    public BadFormatFileException(String message) {
        super(message);
    }
}
