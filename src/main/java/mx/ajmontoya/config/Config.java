package mx.ajmontoya.config;

import mx.ajmontoya.service.VendingMachineFacade;
import mx.ajmontoya.service.VendingMachineFacadeImpl;
import mx.ajmontoya.service.internal.*;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;

@Configuration
@ComponentScan(basePackages = "mx.ajmontoya")
public class Config {

    @Bean
    public ProductService getVendingMachineService(){
        return new ProductServiceImpl();
    }

    @Bean
    public FileLoader getFileLoader(){
        return new FileLoaderImpl();
    }

    @Bean
    public CreditService getCreditService(){
        return new CreditServiceImpl();
    }

    @Bean
    public VendingMachineFacade getVendingMachineFacade() {
        return new VendingMachineFacadeImpl(
                this.getVendingMachineService(),
                this.getFileLoader(),
                this.getCreditService()
        );
    }

}
