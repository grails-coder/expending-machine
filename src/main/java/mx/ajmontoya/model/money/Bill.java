package mx.ajmontoya.model.money;

import java.util.Arrays;
import java.util.Optional;

public enum Bill {
    DOLLAR_2(2),
    DOLLAR_1(1);

    private final int value;

    Bill(int value) {
        this.value = value;
    }

    public int getValue() {
        return value;
    }

    public static Optional<Bill> findByValue(final int value){
        return Arrays
                .stream(values())
                .filter(it -> it.value == value)
                .findFirst();
    }
}
