package mx.ajmontoya.model.money;

import java.util.Arrays;
import java.util.Optional;

public enum Coin {
    CENT_1(1),
    CENT_5(5),
    CENT_10(10),
    CENT_25(25),
    CENT_50(50);

    private final int value;

    Coin(int value) {
        this.value = value;
    }

    public int getValue() {
        return value;
    }

    public static Optional<Coin> findByValue(final int value){
        return Arrays
                .stream(values())
                .filter(it -> it.value == value)
                .findFirst();
    }
}
