package mx.ajmontoya.init;

import mx.ajmontoya.model.money.Bill;
import mx.ajmontoya.model.money.Coin;
import mx.ajmontoya.service.VendingMachineFacade;
import org.springframework.shell.standard.ShellComponent;
import org.springframework.shell.standard.ShellMethod;

import java.util.Optional;

@ShellComponent
public class Commander {

    private final VendingMachineFacade vendingMachineFacade;

    public Commander(final VendingMachineFacade vendingMachineFacade){
        this.vendingMachineFacade = vendingMachineFacade;
    }

    @ShellMethod(value = "Upload products from a CSV file", key = "upload")
    public final String upload(String file){
        return vendingMachineFacade.uploadFromFile(file);
    }

    @ShellMethod(value = "List available products", key = "list")
    public final String list(){
        return vendingMachineFacade.list();
    }

    @ShellMethod(value = "Add coins, accepted coins are: 1, 5, 10, 25, and 50 cents", key = "add cents")
    public final String addCoins(int value){
        Optional<Coin> coin = Coin.findByValue(value);
        if (coin.isPresent()) {
            return vendingMachineFacade.addCoins(coin.get());
        } else {
            return "Coin not valid";
        }
    }

    @ShellMethod(value = "Add bills, accepted bills are: 1 and 2 dollar", key = "add bill")
    public final String addBills(int value){
        Optional<Bill> bill = Bill.findByValue(value);
        if (bill.isPresent()) {
            return vendingMachineFacade.addBills(bill.get());
        } else {
            return "Bill not valid";
        }
    }

    @ShellMethod(value = "Checks available credit", key = "credit")
    public final String availableCredit(){
        return String.format("Current credit: $%s", vendingMachineFacade.showCredit());
    }

    @ShellMethod(value = "Purchase product by indicating desired product code", key = "purchase")
    public final String purchaseProduct(String code){

        return vendingMachineFacade.purchase(code);
    }

    @ShellMethod(value = "Return change", key = "change")
    public final String change(){
        return vendingMachineFacade.change();
    }
    @ShellMethod(value = "Cancel operation", key = "cancel")
    public final String cancelOperation(){
        return vendingMachineFacade.cancel();
    }

}
